


void geant_check(string infile, Long64_t num)
{
  Double_t eE0B;
  Double_t eEB;
  Double_t EenergyBeforeDetectorB;
  Double_t eTotalDepositB;
  Double_t EDepElectronB;
  Int_t EFirstHitDetNbB;
  Double_t EFirstHitDetTimeB;
  Int_t DeadParticlesB;
  Double_t GammaEscapeB;
  Double_t GammaLossB;
  Double_t DLLossB;
  Double_t SELossB;
  Double_t SEDepositB;
  Double_t GammaDepositB;
  Double_t TimeOfFlightB;
  Double_t TimeOfFlight_proton_cmB;
  Double_t elapsedTimeB;
  Int_t bouncesEB;
  vector<Int_t> *Trigger_bouncesEAtTriggerB = new vector<Int_t>;

  vector<Double_t> *tim = new vector<Double_t>;
  vector<Double_t> *energy = new vector<Double_t>;
  vector<Double_t> *x = new vector<Double_t>;
  vector<Double_t> *y = new vector<Double_t>;
  vector<Int_t> *dn = new vector<Int_t>;
  vector<Int_t> *type = new vector<Int_t>;
  vector<Int_t> *parentID = new vector<Int_t>;

  UInt_t nentries;

	TFile fin(infile.c_str());

  if(fin.IsOpen())
  {
	  TTree *t = (TTree*)fin.Get("dynamicTree");

	  t->SetBranchAddress("eE0", &eE0B);
	  t->SetBranchAddress("eE", &eEB);
	  t->SetBranchAddress("EenergyBeforeDetector", &EenergyBeforeDetectorB);
	  t->SetBranchAddress("eTotalDeposit", &eTotalDepositB);
	  t->SetBranchAddress("EDepElectron", &EDepElectronB);
	  t->SetBranchAddress("EFirstHitDetNb", &EFirstHitDetNbB);
	  t->SetBranchAddress("EFirstHitDetTime", &EFirstHitDetTimeB);
	  t->SetBranchAddress("DeadParticles", &DeadParticlesB);
	  t->SetBranchAddress("GammaEscape", &GammaEscapeB);
	  t->SetBranchAddress("GammaLoss", &GammaLossB);
	  t->SetBranchAddress("DLLoss", &DLLossB);
	  t->SetBranchAddress("SELoss", &SELossB);
	  t->SetBranchAddress("SEDeposit", &SEDepositB);
	  t->SetBranchAddress("GammaDeposit", &GammaDepositB);
	  t->SetBranchAddress("TimeOfFlight", &TimeOfFlightB);
	  t->SetBranchAddress("TimeOfFlight_proton_cm", &TimeOfFlight_proton_cmB);
	  t->SetBranchAddress("elapsedTime", &elapsedTimeB);
	  t->SetBranchAddress("bouncesE", &bouncesEB);
	  t->SetBranchAddress("Trigger_bouncesEAtTrigger", &Trigger_bouncesEAtTriggerB);

	  t->SetBranchAddress("Hit_time", &tim);
	  t->SetBranchAddress("Hit_energy", &energy);
	  t->SetBranchAddress("Hit_x", &x);
	  t->SetBranchAddress("Hit_y", &y);
	  t->SetBranchAddress("Hit_detectorNb", &dn);
	  t->SetBranchAddress("Hit_particleType", &type);
	  t->SetBranchAddress("Hit_parentID", &parentID);

    if(num<t->GetEntries())
      nentries = num;
    else
      nentries = t->GetEntries();

    for(Int_t i=100000; i<100020; ++i)
    {
      t->GetEntry(i);

      cout << "\nEntry " << i << ":\n";

      for(Short_t j=0; j<energy->size(); ++j)
        cout << "\nTime: " << (*tim)[j] << ", Energy: " << (*energy)[j] << ", X: "
             << (*x)[j] << ", Y: " << (*y)[j] << ", DN: " << (*dn)[j] << ", Type: "
             << (*type)[j] << ", ParentID: " << (*parentID)[j];

      cout << "\n\neE0: " << eE0B << ", eE: " << eEB << ", EenergyBeforeDetector: "
           << EenergyBeforeDetectorB;

      cout << "\neTotalDeposit: " << eTotalDepositB << ", EDepElectron: "
           << EDepElectronB;

      cout << "\nEFirstHitDetNb: " << EFirstHitDetNbB << ", EFirstHitDetTime: "
           << EFirstHitDetTimeB;

      cout << "\nDeadParticles: " << DeadParticlesB << ", GammaEscape: "
           << GammaEscapeB << ", GammaLoss: " << GammaLossB;

      cout << "\nDLLoss: " << DLLossB << ", SELoss: " << SELossB << ", SEDeposit: "
           << SEDepositB << ", GammaDeposit: " << GammaDepositB;

      cout << "\nTimeOfFlight: " << TimeOfFlightB << ", TimeOfFlight_proton_cm: "
           << TimeOfFlight_proton_cmB;

      cout << "\nelapsedTime: " << elapsedTimeB << ", bouncesE: " << bouncesEB;

      cout << "\nTrigger_bouncesEAtTrigger:";

      for(Int_t j=0; j<Trigger_bouncesEAtTriggerB->size(); ++j)
        cout << ' ' << (*Trigger_bouncesEAtTriggerB)[j];

      cout << '\n';
    }
  }
  else
    cout << "\nCannot Open Input File\n\n";

  delete Trigger_bouncesEAtTriggerB;
  delete tim;
  delete energy;
  delete x;
  delete y;
  delete dn;
  delete type;
  delete parentID;
}
