


UChar_t pix_select(Double_t, Double_t);

void convert_geant_basic(string infile, string outfile, Long64_t initNum, Long64_t num, Char_t modeFlag = {})
{
	Int_t tempFlag;
  UChar_t pix;
  Int_t bd;
	UChar_t ch;
  ULong64_t tstamp;
  UShort_t engy;
	Short_t bdChPixel[256];
  for(Short_t i=0; i<256; ++i)
          bdChPixel[i] = 0;
  bdChPixel[39] = 1;
  bdChPixel[40] = 2;
  bdChPixel[41] = 3;
  bdChPixel[50] = 4;
  bdChPixel[51] = 5;
  bdChPixel[52] = 6;
  bdChPixel[53] = 7;
  bdChPixel[62] = 9;
  bdChPixel[63] = 10;
  bdChPixel[64] = 11;
  bdChPixel[65] = 12;
  bdChPixel[66] = 13;
  bdChPixel[75] = 14;
  bdChPixel[76] = 15;
  bdChPixel[77] = 17;
  bdChPixel[78] = 18;
  bdChPixel[87] = 19;
  bdChPixel[88] = 20;
  bdChPixel[89] = 21;
  bdChPixel[167] = 25;
  bdChPixel[168] = 26;
  bdChPixel[169] = 27;
  bdChPixel[178] = 28;
  bdChPixel[179] = 29;
  bdChPixel[180] = 30;
  bdChPixel[181] = 31;
  bdChPixel[190] = 33;
  bdChPixel[191] = 34;
  bdChPixel[192] = 35;
  bdChPixel[193] = 36;
  bdChPixel[194] = 37;
  bdChPixel[203] = 38;
  bdChPixel[204] = 39;
  bdChPixel[205] = 41;
  bdChPixel[206] = 42;
  bdChPixel[215] = 43;
  bdChPixel[216] = 44;
  bdChPixel[217] = 45;

  Double_t sortD;
  Int_t sortI;
  Long64_t nentries;

  vector<Double_t> *tim = nullptr;
  vector<Double_t> *energy = nullptr;
  vector<Double_t> *x = nullptr;
  vector<Double_t> *y = nullptr;
  vector<Int_t> *dn = nullptr;
  vector<Int_t> *type = nullptr;
  vector<Int_t> *parentID = nullptr;
	Double_t eEnergy0{};

	TFile fin(infile.c_str());

  if(fin.IsOpen())
  {
	  TTree *t = dynamic_cast<TTree*>(fin.Get("dynamicTree"));
	  t->SetBranchAddress("Hit_time", &tim);
	  t->SetBranchAddress("Hit_energy", &energy);
	  t->SetBranchAddress("Hit_x", &x);
	  t->SetBranchAddress("Hit_y", &y);
	  t->SetBranchAddress("Hit_detectorNb", &dn);
	  t->SetBranchAddress("Hit_particleType", &type);
	  t->SetBranchAddress("Hit_parentID", &parentID);
	  t->SetBranchAddress("eE0", &eEnergy0);

    if(initNum + num <= t->GetEntries())
      nentries = {initNum + num};
    else
		{
      nentries = {t->GetEntries()};
			std::cout << "Warning: Number of Entries Truncated as EOF Has Been Reached in Input File: " << infile << '\n';
		}

		if(nentries < 1)
			std::cout << "Warning: No Events Have Been Converted as the Initial Event Number Exceeds the Number of Events "
			"in Input File: " << infile << "\nWarning: Only a Global Timestamp Has Been Written to Output File: " << outfile << '\n';

    ofstream fout(outfile, ofstream::binary);

    if(fout.good())
    {
    	fout.write((Char_t*)&tstamp, 8);

      for(Long64_t i{initNum}; i<nentries; ++i)
      {
        t->GetEntry(i);

        for(Int_t j=1; j>0;)
        {
          j=0;

          for(Int_t k=1; k<tim->size(); ++k)
            if((*tim)[k]<(*tim)[k-1])
            {
              sortD=(*tim)[k];
              (*tim)[k]=(*tim)[k-1];
              (*tim)[k-1]=sortD;

              sortD=(*energy)[k];
              (*energy)[k]=(*energy)[k-1];
              (*energy)[k-1]=sortD;

              sortD=(*x)[k];
              (*x)[k]=(*x)[k-1];
              (*x)[k-1]=sortD;

              sortD=(*y)[k];
              (*y)[k]=(*y)[k-1];
              (*y)[k-1]=sortD;

              sortI=(*dn)[k];
              (*dn)[k]=(*dn)[k-1];
              (*dn)[k-1]=sortI;

              sortI=(*type)[k];
              (*type)[k]=(*type)[k-1];
              (*type)[k-1]=sortI;

              sortI=(*parentID)[k];
              (*parentID)[k]=(*parentID)[k-1];
              (*parentID)[k-1]=sortI;

              ++j;
            }
        }

				tempFlag = {};

        for(Int_t j{}; j<tim->size(); ++j)
        {
          if((*dn)[j]==1)
            pix = pix_select((*x)[j], (*y)[j]);
          else if((*dn)[j]==-1)
            pix = pix_select((*x)[j], (*y)[j]) + 128;

					bd = bdChPixel[pix]/8;
					ch = bdChPixel[pix]%8;
          tstamp = 166667*i + (*tim)[j]/4;
          engy = (*energy)[j];

					if(!modeFlag || (modeFlag && ((*type)[j] == 1 || (!tempFlag && (*type)[j] != 1))))
					{
						if(modeFlag && (*type)[j] != 1)
						{
							engy = eEnergy0;
							tempFlag = {1};
						}

          	fout.write((Char_t*)&bd, 4);
          	fout.write((Char_t*)&ch, 1);
          	fout.write((Char_t*)&tstamp, 8);
         		fout.write((Char_t*)&engy, 2);
					}
        }
      }
    }
    else
      cout << "\nCannot Open Output File\n\n";
  }
  else
    cout << "\nCannot Open Input File\n\n";
}

//-------------------------------------------------------------------------------------------------

UChar_t pix_select(Double_t x, Double_t y)
{
  Double_t px[6] = {22.998,68.993,114.987,160.982,206.977,252.972};
  Double_t py[7] = {23.334,70.001,116.668,163.334,210.002,256.668};
  UChar_t p[2];

  if(x<-px[5])
    p[0]=0;
  else if(x<-px[4])
    p[0]=1;
  else if(x<-px[3])
    p[0]=2;
  else if(x<-px[2])
    p[0]=3;
  else if(x<-px[1])
    p[0]=4;
  else if(x<-px[0])
    p[0]=5;
  else if(x<px[0])
    p[0]=6;
  else if(x<px[1])
    p[0]=7;
  else if(x<px[2])
    p[0]=8;
  else if(x<px[3])
    p[0]=9;
  else if(x<px[4])
    p[0]=10;
  else if(x<px[5])
    p[0]=11;
  else
    p[0]=12;

  switch(p[0])
  {
    case 0:
      if(y>py[2])
        p[1]=1;
      else if(y>py[1])
        p[1]=2;
      else if(y>py[0])
        p[1]=3;
      else if(y>-py[0])
        p[1]=4;
      else if(y>-py[1])
        p[1]=5;
      else if(y>-py[2])
        p[1]=6;
      else
        p[1]=7;
      break;
    case 1:
      if(y>py[2]+23.3335)
        p[1]=8;
      else if(y>py[1]+23.3335)
        p[1]=9;
      else if(y>py[0]+23.3335)
        p[1]=10;
      else if(y>0)
        p[1]=11;
      else if(y>-py[1]+23.3335)
        p[1]=12;
      else if(y>-py[2]+23.3335)
        p[1]=13;
      else if(y>-py[3]+23.3335)
        p[1]=14;
      else
        p[1]=15;
      break;
    case 2:
      if(y>py[3])
        p[1]=16;
      else if(y>py[2])
        p[1]=17;
      else if(y>py[1])
        p[1]=18;
      else if(y>py[0])
        p[1]=19;
      else if(y>-py[0])
        p[1]=20;
      else if(y>-py[1])
        p[1]=21;
      else if(y>-py[2])
        p[1]=22;
      else if(y>-py[3])
        p[1]=23;
      else
        p[1]=24;
      break;
    case 3:
      if(y>py[3]+23.3335)
        p[1]=25;
      else if(y>py[2]+23.3335)
        p[1]=26;
      else if(y>py[1]+23.3335)
        p[1]=27;
      else if(y>py[0]+23.3335)
        p[1]=28;
      else if(y>0)
        p[1]=29;
      else if(y>-py[1]+23.3335)
        p[1]=30;
      else if(y>-py[2]+23.3335)
        p[1]=31;
      else if(y>-py[3]+23.3335)
        p[1]=32;
      else if(y>-py[4]+23.3335)
        p[1]=33;
      else
        p[1]=34;
      break;
    case 4:
      if(y>py[4])
        p[1]=35;
      else if(y>py[3])
        p[1]=36;
      else if(y>py[2])
        p[1]=37;
      else if(y>py[1])
        p[1]=38;
      else if(y>py[0])
        p[1]=39;
      else if(y>-py[0])
        p[1]=40;
      else if(y>-py[1])
        p[1]=41;
      else if(y>-py[2])
        p[1]=42;
      else if(y>-py[3])
        p[1]=43;
      else if(y>-py[4])
        p[1]=44;
      else
        p[1]=45;
      break;
    case 5:
      if(y>py[4]+23.3335)
        p[1]=46;
      else if(y>py[3]+23.3335)
        p[1]=47;
      else if(y>py[2]+23.3335)
        p[1]=48;
      else if(y>py[1]+23.3335)
        p[1]=49;
      else if(y>py[0]+23.3335)
        p[1]=50;
      else if(y>0)
        p[1]=51;
      else if(y>-py[1]+23.3335)
        p[1]=52;
      else if(y>-py[2]+23.3335)
        p[1]=53;
      else if(y>-py[3]+23.3335)
        p[1]=54;
      else if(y>-py[4]+23.3335)
        p[1]=55;
      else if(y>-py[5]+23.3335)
        p[1]=56;
      else
        p[1]=57;
      break;
    case 6:
      if(y>py[5])
        p[1]=58;
      else if(y>py[4])
        p[1]=59;
      else if(y>py[3])
        p[1]=60;
      else if(y>py[2])
        p[1]=61;
      else if(y>py[1])
        p[1]=62;
      else if(y>py[0])
        p[1]=63;
      else if(y>-py[0])
        p[1]=64;
      else if(y>-py[1])
        p[1]=65;
      else if(y>-py[2])
        p[1]=66;
      else if(y>-py[3])
        p[1]=67;
      else if(y>-py[4])
        p[1]=68;
      else if(y>-py[5])
        p[1]=69;
      else
        p[1]=70;
      break;
    case 7:
      if(y>py[4]+23.3335)
        p[1]=71;
      else if(y>py[3]+23.3335)
        p[1]=72;
      else if(y>py[2]+23.3335)
        p[1]=73;
      else if(y>py[1]+23.3335)
        p[1]=74;
      else if(y>py[0]+23.3335)
        p[1]=75;
      else if(y>0)
        p[1]=76;
      else if(y>-py[1]+23.3335)
        p[1]=77;
      else if(y>-py[2]+23.3335)
        p[1]=78;
      else if(y>-py[3]+23.3335)
        p[1]=79;
      else if(y>-py[4]+23.3335)
        p[1]=80;
      else if(y>-py[5]+23.3335)
        p[1]=81;
      else
        p[1]=82;
      break;
    case 8:
      if(y>py[4])
        p[1]=83;
      else if(y>py[3])
        p[1]=84;
      else if(y>py[2])
        p[1]=85;
      else if(y>py[1])
        p[1]=86;
      else if(y>py[0])
        p[1]=87;
      else if(y>-py[0])
        p[1]=88;
      else if(y>-py[1])
        p[1]=89;
      else if(y>-py[2])
        p[1]=90;
      else if(y>-py[3])
        p[1]=91;
      else if(y>-py[4])
        p[1]=92;
      else
        p[1]=93;
      break;
    case 9:
      if(y>py[3]+23.3335)
        p[1]=94;
      else if(y>py[2]+23.3335)
        p[1]=95;
      else if(y>py[1]+23.3335)
        p[1]=96;
      else if(y>py[0]+23.3335)
        p[1]=97;
      else if(y>0)
        p[1]=98;
      else if(y>-py[1]+23.3335)
        p[1]=99;
      else if(y>-py[2]+23.3335)
        p[1]=100;
      else if(y>-py[3]+23.3335)
        p[1]=101;
      else if(y>-py[4]+23.3335)
        p[1]=102;
      else
        p[1]=103;
      break;
    case 10:
      if(y>py[3])
        p[1]=104;
      else if(y>py[2])
        p[1]=105;
      else if(y>py[1])
        p[1]=106;
      else if(y>py[0])
        p[1]=107;
      else if(y>-py[0])
        p[1]=108;
      else if(y>-py[1])
        p[1]=109;
      else if(y>-py[2])
        p[1]=110;
      else if(y>-py[3])
        p[1]=111;
      else
        p[1]=112;
      break;
    case 11:
      if(y>py[2]+23.3335)
        p[1]=113;
      else if(y>py[1]+23.3335)
        p[1]=114;
      else if(y>py[0]+23.3335)
        p[1]=115;
      else if(y>0)
        p[1]=116;
      else if(y>-py[1]+23.3335)
        p[1]=117;
      else if(y>-py[2]+23.3335)
        p[1]=118;
      else if(y>-py[3]+23.3335)
        p[1]=119;
      else
        p[1]=120;
      break;
    case 12:
      if(y>py[2])
        p[1]=121;
      else if(y>py[1])
        p[1]=122;
      else if(y>py[0])
        p[1]=123;
      else if(y>-py[0])
        p[1]=124;
      else if(y>-py[1])
        p[1]=125;
      else if(y>-py[2])
        p[1]=126;
      else
        p[1]=127;
      break;
  }

  return p[1];
}
